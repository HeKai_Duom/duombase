//
//  ViewController.swift
//  DuomBase
//
//  Created by HeKai_Duom on 08/25/2022.
//  Copyright (c) 2022 HeKai_Duom. All rights reserved.
//

import UIKit
import DuomBase

class ViewController: UIViewController {

    private lazy var layout = UICollectionViewFlowLayout().then {
        $0.itemSize = CGSize(width: 50, height: 50)
        $0.minimumLineSpacing = 10
        $0.minimumLineSpacing = 10
    }
    
    private lazy var colletionView: UICollectionView = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let VM = ViewModel()
        VM.refresher = colletionView.refresher
        colletionView.refresher?.setting(style: .dark, type: .header)
        colletionView.refresher?.setting(style: .light, type: .footer)
        
        colletionView.frame = CGRect.init(x: 1, y: 1, width: UIScreen.screenWidth, height: 100)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        toast("哈哈哈")
        
    }

}

