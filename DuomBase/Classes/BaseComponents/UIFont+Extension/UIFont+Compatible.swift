//
//  UIFont+Compatible.swift
//  DuomBase
//
//  Created by kuroky on 2022/9/2.
//

import Foundation
import UIKit

public enum CompatibleFontWeight: String, CaseIterable {
    case ultraLight = "HelveticaNeue-UltraLight"
    case thin = "HelveticaNeue-Thin"
    case light = "HelveticaNeue-Light"
    case regular = "HelveticaNeue"
    case medium = "HelveticaNeue-Medium"
    case semibold = "Helvetica-Bold"
    case bold = "HelveticaNeue-Bold"
    case heavy = "HelveticaNeue-CondensedBold"
    case black = "HelveticaNeue-CondensedBlack"

    @available(iOS 8.2, *)
    public var systemWeight: UIFont.Weight {
        switch self {
        case .ultraLight:
            return UIFont.Weight.ultraLight
        case .thin:
            return UIFont.Weight.thin
        case .light:
            return UIFont.Weight.light
        case .regular:
            return UIFont.Weight.regular
        case .medium:
            return UIFont.Weight.medium
        case .semibold:
            return UIFont.Weight.semibold
        case .bold:
            return UIFont.Weight.bold
        case .heavy:
            return UIFont.Weight.heavy
        case .black:
            return .black
        }
    }
}

public extension UIFont {
    static var fontSizeMultiplier: CGFloat = min(1.5, max(1.f, UIScreen.screenWidth / 375.f))

    static func appFont(ofSize size: CGFloat, weight: CompatibleFontWeight = .regular) -> UIFont {
        if #available(iOS 8.2, *) {
            return UIFont.systemFont(ofSize: size, weight: weight.systemWeight)
        } else if let font = UIFont(name: weight.rawValue, size: size) {
            return font
        } else {
            return .systemFont(ofSize: size)
        }
    }

    static func appFont(ofScaleBase size: CGFloat, weight: CompatibleFontWeight = .regular) -> UIFont {
        let font = UIFont.appFont(ofSize: size, weight: weight)
        return UIFont(descriptor: font.fontDescriptor, size: ceil(font.pointSize * fontSizeMultiplier))
    }

    var shtScale: UIFont {
        return UIFont(descriptor: fontDescriptor, size: ceil(pointSize * UIFont.fontSizeMultiplier))
    }
}
