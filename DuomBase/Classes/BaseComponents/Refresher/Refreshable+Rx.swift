//
//  Refreshable+Rx.swift
//  DuomBase
//
//  Created by kuroky on 2022/9/2.
//

import Foundation
import MJRefresh
import RxCocoa
import RxSwift

public extension Reactive where Base: UIScrollView {
    var headerRefresh: ControlEvent<Refresher> {
        let source = lazyAssociatedObservable(forKey: &AssociatedKey.header_event_key) {
            Observable<Refresher>.create {  [weak scrollView = self.base] observer -> Disposable in
                guard let scrollView = scrollView else { return Disposables.create() }
                
                scrollView.setupHeader {
                    observer.onNext($0)
                }
                
                return Disposables.create()
            }
            .take(until: self.deallocated)
            .share()
        }
        return ControlEvent(events: source)
    }
    
    var footerRefresh: ControlEvent<Refresher> {
        let source = lazyAssociatedObservable(forKey: &AssociatedKey.footer_event_key) {
            Observable<Refresher>.create { [weak scrollView = self.base] observer -> Disposable in
                guard let scrollView = scrollView else { return Disposables.create() }
                
                scrollView.setupFooter { observer.onNext($0) }
                
                return Disposables.create()
            }
            .take(until: self.deallocated)
            .share()
        }
        
        return ControlEvent(events: source)
    }
    
    var headerFooterRefresh: ControlEvent<(RefreshType, Refresher)> {
        let source = lazyAssociatedObservable(forKey: &AssociatedKey.header_footer_event_key) {
            Observable<(RefreshType, Refresher)>.create { [weak scrollView = self.base] observer -> Disposable in
                guard let scrollView = scrollView else { return Disposables.create() }
                
                scrollView.setupHeaderAndFooter { observer.onNext(($0, $1)) }
                
                return Disposables.create()
            }
            .take(until: self.deallocated)
            .share()
        }
        
        return ControlEvent(events: source)
    }
    
    var endRefresh: Binder<RefreshState> {
        return Binder(base) { $0.refresher?.endRefresh(with: $1) }
    }
    
    private func lazyAssociatedObservable<T>(forKey key: UnsafeRawPointer, default: () -> Observable<T>) -> Observable<T> {
        if let object = objc_getAssociatedObject(base, key) as? Observable<T> {
            return object
        }
        
        let object = `default`()
        
        objc_setAssociatedObject(base, key, object, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        
        return object
    }
    
    
}

private enum AssociatedKey {
    static var header_event_key: UInt8 = 0
    static var footer_event_key: UInt8 = 0
    static var header_footer_event_key: UInt8 = 0
}
