//
//  DuomNormalRefreshHeader.swift
//  DuomBase
//
//  Created by kuroky on 2022/9/2.
//

import Foundation
import MJRefresh
import UIKit

class DuomNormalRefreshHeader: MJRefreshNormalHeader {
    open override var state: MJRefreshState {
        willSet {
            superview?.bringSubviewToFront(self)
        }

        didSet {
            if #available(iOS 10.0, *), state == .pulling {
                let generator = UIImpactFeedbackGenerator(style: .medium)
                generator.prepare()
                generator.impactOccurred()
            }
        }
    }

    open override func prepare() {
        super.prepare()

        lastUpdatedTimeLabel?.isHidden = true

        setTitle("下拉刷新", for: .idle)
        setTitle("释放刷新", for: .pulling)
        setTitle("释放刷新", for: .willRefresh)
        setTitle("正在刷新", for: .refreshing)
    }
}
