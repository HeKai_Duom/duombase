//
//  BaseViewController.swift
//  Duom
//
//  Created by kuroky on 2022/8/23.
//


import Foundation

public extension Array {
    func element(_ index: Int) -> Element? {
        guard index >= 0, count > index else {
            return nil
        }
        return self[index]
    }
}
