//
//  Toaster.swift
//  DuomBase
//
//  Created by kuroky on 2022/8/31.
//

import MBProgressHUD

public func toast(_ message: String?, delay: TimeInterval = 2) {
    guard let targetView = UIApplication.shared.keyWindow else { return }

    MBProgressHUD.hide(for: targetView, animated: false)

    guard let message = message else { return }
    guard message.count > 0 else { return }

    let hud = MBProgressHUD.showAdded(to: targetView, animated: true).then {
        $0.mode = .text
        $0.bezelView.style = .solidColor
        $0.bezelView.color = UIColor.init(hex: "0x060815", alpha: 0.8)
        $0.contentColor = .white

        $0.label.text = message
        $0.label.font = UIFont.systemFont(ofSize: 15)
        $0.label.numberOfLines = 0
        $0.margin = 15
        $0.isUserInteractionEnabled = false
    }

    hud.hide(animated: true, afterDelay: delay)
}
