//
//  BaseViewController.swift
//  Duom
//
//  Created by kuroky on 2022/8/23.
//

import Foundation
import UIKit

public class RoundImageView: UIImageView {
    public var color: String? = "#AA4E5E76"
    public var borderWidth: CGFloat = 1
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        rounded(color: color, borderWidth: borderWidth)
    }
}
