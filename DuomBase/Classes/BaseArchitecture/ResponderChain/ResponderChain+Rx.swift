//
//  ViewType.swift
//  Duom
//
//  Created by kuroky on 2022/8/23.
//


import RxCocoa
import RxSwift

public extension Reactive where Base: UIResponder {
    var routerEvents: Binder<ResponderChainEvent> {
        return Binder(self.base) { responder, event in
            responder.emitRouterEvent(event)
        }
    }
}
