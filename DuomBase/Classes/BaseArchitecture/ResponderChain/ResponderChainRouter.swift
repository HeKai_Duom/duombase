//
//  ViewType.swift
//  Duom
//
//  Created by kuroky on 2022/8/23.
//

import RxSwift
import RxOptional

public extension ResponderChainWrapper where Self: ResponderChainStorage {
    var events: Observable<Event> {
        return _events.map { $0 as? Event }.filterNil().share()
    }
}

public extension ResponderChainStorage {
    var _events: PublishSubject<ResponderChainEvent> {
        return lazyInstanceObservable(&rcp_events_key) { PublishSubject<ResponderChainEvent>() }
    }

    private func lazyInstanceObservable<T: AnyObject>(_ key: UnsafeRawPointer, createCachedObservable: () -> T) -> T {
        if let value = objc_getAssociatedObject(self, key) {
            return value as! T
        }

        let observable = createCachedObservable()

        objc_setAssociatedObject(self, key, observable, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)

        return observable
    }
}

private var rcp_events_key: UInt8 = 0
